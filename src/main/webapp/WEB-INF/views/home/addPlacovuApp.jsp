<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Placovu Ping App</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script type="text/javascript">
  function editRecord(id){
	    var f=document.form;
	    f.method="post";
	    f.action='edit.jsp?id='+id;
	    f.submit();
	}
	function deleteRecord(id){
	    var f=document.form;
	    f.method="post";
	    f.action='delete.jsp?id='+id;
	    f.submit();
	    }
</script>
</head>
<body>
	<div class="container">
		 <div class="panel panel-default">
	     	<div class="panel-heading"><h2>Placovu Ping App</h2></div>
	     	<div class ="panel-body-pabel-primary"></div>
				<div class="panel-body panel-primary">
				    <form action="<%=request.getContextPath().toString()%>/SetPlacovuAppAddForm" method="post">
					  Service Name:
					  <input type="text" name="servciceName"><br>
					  Service Type:
					  <input type="text" name="serviceType"><br>
					  Domain Name:
					  <input type="text" name="domainName"><br>
					  Ip Address:
					  <input type="text" name="ipAddress"><br>
					  <br>
					  <input type="submit" value="Submit">
					</form>
				 </div>
	  	</div>
	</div> 
</body>
</html>