<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
 
pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Placovu Ping App</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script type="text/javascript">
	function getPlacovuApp()
	{

		 $.ajax({
			 url: "<%=request.getContextPath().toString()%>/placovuapp",
			 method: "get",
		 	 dataType : "json",
		 	 method: "get",
		 	 global : false,
			 success: function(appData){
				 $("#patientProfileList").html("");
				 var html = "";
                 var size = appData.length;
                 for(var i=0;i<size;i++)
                 {
                	 html+="<tr>"
     			           +"  <td>"+appData[i].serviceName+"</td>"
     			           +"  <td>"+appData[i].serviceType+"</td>"
     			           +"  <td>"+appData[i].domainName+"</td>"
     			           +"  <td>"+appData[i].ipAddress+"</td>";
     			           if(appData[i].statusCode ==404)
     			        	 html+="  <td>Down</td>";
     			           else
     			           html+="  <td>Up</td>";
     			           html+="  <td>"+appData[i].statusCode+"</td>";
     		               html+= '<td><a href="<%=request.getContextPath().toString()%>/PlacovuAppEditForm">Edit</a>'
     		               html+= '<td><a href="<%=request.getContextPath().toString()%>/PlacovuAppDeleteForm">Delete</a>'
    			      	   +"</tr>";	
                 }
                 $("#patientProfileList").html(html);
		 	 },
		 	 error: function(result){
		 	 }
		});
	}
	
	$(document).ready(function(){
		getPlacovuApp();
	});

</script>
</head>
<body>
	<div class="container">
		 <div class="panel panel-default">
	     	<div class="panel-heading"><h2>Placovu Ping App</h2></div>
	     	<div class ="panel-body-pabel-primary"></div>
	     	<a href="<%=request.getContextPath().toString()%>/PlacovuAppAddForm">Add New Services</a>
				<div class="panel-body panel-primary">
		    	 <table class="table table-striped">
				    <thead>
				      <tr>
				        <th>Service Name</th>
				        <th>Service Type</th>
				        <th>Domain Name</th>
				        <th>Ip Address</th>
				        <th> Status </th>
				        <th>Status Code</th>
				        <th>Edit Services</th>
				        <th>Delete Services</th>
				      </tr>
				    </thead>
				    <tbody id="patientProfileList">
				    </tbody>
				  </table>
				 </div>
	  	</div>
	</div>
</body>
</html>