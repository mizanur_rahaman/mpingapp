package com.placovupingapp.mailmanager;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.placovupingapp.manager.ServiceAliveTester;

@Service
public class EmailSender {
	@Autowired Email email;
	private static final Logger logger = Logger.getLogger(EmailSender.class);
	public void sendEmail(EmailMessage emailMessage)
	{
		logger.info(email.getSenderEmail() + " " + email.getHost() + " " + email.getPort());
		final String username = email.getSenderEmail();
		final String password = email.getSenderPassword();;

		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "false");
		props.put("mail.smtp.host", email.getHost());
		props.put("mail.smtp.port", email.getPort());
		//props.put("mail.smtp.ssl.trust","smtp.emailsrvr.com");
		

		Session session = Session.getInstance(props,
		  new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		  });

		try {

			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(email.getSenderEmail()));
			message.setRecipients(Message.RecipientType.TO,
				InternetAddress.parse(emailMessage.getReceiverEmail()));
			message.setSubject(emailMessage.getSubject());
			message.setText(emailMessage.getMessage());

			Transport.send(message);

			logger.info("email send");

		}
		catch (MessagingException e) {
			e.printStackTrace();
			logger.info(e.getMessage());
		}
	}
}
