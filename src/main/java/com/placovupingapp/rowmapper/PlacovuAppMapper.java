package com.placovupingapp.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.placovupingapp.dto.PlacovuAppDto;

public class PlacovuAppMapper implements RowMapper<PlacovuAppDto>{

	public PlacovuAppDto mapRow(ResultSet rs, int rowNumber) throws SQLException {
		// TODO Auto-generated method stub
		PlacovuAppDto placovuServiceDto = new PlacovuAppDto();
		placovuServiceDto.setId(rs.getLong("id"));
		placovuServiceDto.setServiceName(rs.getString("service_name"));
		placovuServiceDto.setServiceType(rs.getString("service_type"));
		placovuServiceDto.setIpAddress(rs.getString("ip_address"));
		placovuServiceDto.setDomainName(rs.getString("domain_name"));
		placovuServiceDto.setStatusCode(rs.getInt("status_code"));
        return placovuServiceDto;
	}
}
