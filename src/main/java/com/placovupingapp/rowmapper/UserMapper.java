package com.placovupingapp.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.placovupingapp.dto.UserDto;


public class UserMapper implements RowMapper<UserDto>{

	public UserDto mapRow(ResultSet rs, int rowNumber) throws SQLException {
		// TODO Auto-generated method stub
		UserDto userDto = new UserDto();
		userDto.setId(rs.getLong("id"));
		userDto.setFirstName(rs.getString("first_name"));
		userDto.setLastName(rs.getString("last_name"));
		userDto.setEmailAddress(rs.getString("email_address"));
		userDto.setPassword(rs.getString("password"));
        return userDto;
	}
}
