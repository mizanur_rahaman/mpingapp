package com.placovupingapp.dto;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;

@Configuration
@PropertySource(value = "classpath:placovuapp.properties")

public class PlacovuAppRestDto {
	
	@Value("${ontrackhealth.url}") 
	private String ontrackHealthUrl;
	
	@Value("${ontrackhealth.email_serviceName}") 
	private String emailServiceName;
	
	@Value("${ontrackhealth.database_resturl}") 
	private String ontrackDatabaseUrl;

	public String getOntrackHealthUrl() {
		return ontrackHealthUrl;
	}

	public void setOntrackHealthUrl(String ontrackHealthUrl) {
		this.ontrackHealthUrl = ontrackHealthUrl;
	}

	public String getEmailServiceName() {
		return emailServiceName;
	}

	public void setEmailServiceName(String emailServiceName) {
		this.emailServiceName = emailServiceName;
	}

	public String getOntrackDatabaseUrl() {
		return ontrackDatabaseUrl;
	}

	public void setOntrackDatabaseUrl(String ontrackDatabaseUrl) {
		this.ontrackDatabaseUrl = ontrackDatabaseUrl;
	}
	
	
	@Bean
    public static PropertySourcesPlaceholderConfigurer  propertySourcesPlaceholderConfigurer() {
        return new PropertySourcesPlaceholderConfigurer();
    }
	

}