package com.placovupingapp.controller;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import com.placovupingapp.dto.PlacovuAppDto;
import com.placovupingapp.service.PlacovuAppService;

@Controller
public class LoginController {
	@Autowired PlacovuAppService placovuAppService;
	@RequestMapping("/")
	public ModelAndView login() { 

		ModelAndView mv = new ModelAndView("home/PingApp");
		return mv;
	}	
}
