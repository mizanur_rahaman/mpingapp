package com.placovupingapp.controller;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import com.placovupingapp.dto.PlacovuAppDto;
import com.placovupingapp.form.PlacovuAppForm;
import com.placovupingapp.service.PlacovuAppService;
@RestController
public class PlacovuAppController {
	@Autowired PlacovuAppService placovuAppService;
	
	@RequestMapping(value="/placovuapp",method=RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
	public   List<PlacovuAppDto> getPlacovuAppList(HttpServletRequest request,HttpServletResponse response) 
	{
		//List<PlacovuAppDto>  placovuAppList =   placovuAppService.getPlacovuServiceDtos();

		//return placovuAppList;	
		return new ArrayList<PlacovuAppDto>();
	}
	
	@RequestMapping("/PlacovuAppAddForm")
	public ModelAndView getPlacovuAppForm() { 

		ModelAndView mv = new ModelAndView("home/addPlacovuApp");
		return mv;
	}
	
	@RequestMapping(value ="/PlacovuAppEditForm", method=RequestMethod.POST)
	public ModelAndView getPlacovuAppEditForm(@ModelAttribute  PlacovuAppForm placovuAppAddForm) { 
		ModelAndView mv = new ModelAndView("home/edit");
		return mv;
	}
	
	
	@RequestMapping("/PlacovuAppDeleteForm")
	public ModelAndView getPlacovuAppDeleteForm() { 

		ModelAndView mv = new ModelAndView("home/PingApp");
		return mv;
	}
	
	@RequestMapping(value="/SetPlacovuAppAddForm",method=RequestMethod.POST)
	public ModelAndView setdataPlacovuAppForm(@ModelAttribute  PlacovuAppForm placovuAppAddForm) { 
		PlacovuAppDto  placovuAppDto = new PlacovuAppDto();
		placovuAppDto.setServiceName(placovuAppAddForm.getServciceName());
		placovuAppDto.setServiceType(placovuAppAddForm.getServiceType());
		placovuAppDto.setDomainName(placovuAppAddForm.getDomainName());
		placovuAppDto.setIpAddress(placovuAppAddForm.getIpAddress());
		System.out.println("output");
		System.out.println(placovuAppAddForm.getServciceName()+" "+placovuAppAddForm.getServiceType()+" "+placovuAppAddForm.getDomainName()+" "+placovuAppAddForm.getIpAddress());
		placovuAppService.InsertPlacovuServiceDtos(placovuAppDto);
		ModelAndView mv = new ModelAndView("home/PingApp");
		return mv;
	}	
}
