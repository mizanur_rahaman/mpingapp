package com.placovupingapp.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import com.placovupingapp.dao.UserDao;
import com.placovupingapp.dto.UserDto;

@Service
public class UserService {
	@Autowired JdbcTemplate jdbcTemplate;
	@Autowired UserDao userDao;
	public void getUserDtos()
	{
		userDao.setJdbcTemplate(jdbcTemplate);
		List<UserDto> userDtos = userDao.getUserDtos();
		for(int i=0;i<userDtos.size();i++)
		{
			UserDto userDto = userDtos.get(i);
			System.out.println("email: "+userDto.getEmailAddress() + " pass: " + userDto.getPassword());
		}
	}
}
