package com.placovupingapp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import com.placovupingapp.dao.PlacovuAppDao;
import com.placovupingapp.dto.PlacovuAppDto;
import com.placovupingapp.manager.ServiceAliveTester;

@Service
public class PlacovuAppService {
	//@Autowired JdbcTemplate jdbcTemplate;
	@Autowired PlacovuAppDao placovuServiceDao;
	 @Autowired ServiceAliveTester  serviceAliveTester;
	public  List<PlacovuAppDto>  getPlacovuServiceDtos()
	{
		//placovuServiceDao.setJdbcTemplate(jdbcTemplate);
		List<PlacovuAppDto> dtos = placovuServiceDao.getPlacovuServiceDtos();
		return dtos;
	}
	public void testAppService()
	{
		//placovuServiceDao.setJdbcTemplate(jdbcTemplate);
		List<PlacovuAppDto> dtos = placovuServiceDao.getPlacovuServiceDtos();
		  
		  
		for(int i=0;i<dtos.size();i++)
		{
			PlacovuAppDto dto = dtos.get(i);
			//System.out.println("domain: "+dto.getDomainName() + " service name: " + dto.getServiceName());
			if(dto.getServiceType().equalsIgnoreCase("web_service"))
					serviceAliveTester.testWebSeverRunning(placovuServiceDao,dto);
			else if(dto.getServiceType().equalsIgnoreCase("windows_service"))
				 serviceAliveTester.testWindowsService(placovuServiceDao,dto);
		}
	}
	
	public void  InsertPlacovuServiceDtos(PlacovuAppDto placovuAppDto)
	{
		//placovuServiceDao.setJdbcTemplate(jdbcTemplate);
		placovuServiceDao.insertPlacovuApp(placovuAppDto);
	}
	public void testWebSeverRunningUsingRestTemplate(String url, boolean databaseUrl)
	{
		serviceAliveTester.testWebSeverRunningUsingRestTemplate(url,databaseUrl);
	}
	

	public void testWindowsService(String emailServiceName) {
		serviceAliveTester.testWindowsService(emailServiceName);
	}
	
}
