package com.placovupingapp.dao;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;
import com.placovupingapp.dto.PlacovuAppDto;
import com.placovupingapp.dto.UserDto;
import com.placovupingapp.rowmapper.PlacovuAppMapper;
import com.placovupingapp.rowmapper.UserMapper;
@Repository
public class PlacovuAppDao {
	private JdbcTemplate jdbcTemplate;
    public void setJdbcTemplate(JdbcTemplate template) {  
        this.jdbcTemplate = template;  
    } 
	public List<PlacovuAppDto> getPlacovuServiceDtos() {
		List<PlacovuAppDto> dtos = new ArrayList<PlacovuAppDto>();
		try{
			String sql = "select *from placovu_app";
			dtos = jdbcTemplate.query(sql,new PlacovuAppMapper());
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return dtos;  
	}
	
	public void updatePlacovuApp(final PlacovuAppDto placovuAppDto)
	{
		String sql = "update placovu_app set status_code = ? where id = ?";
		System.out.println(placovuAppDto.getStatusCode());
		System.out.println(sql);
		jdbcTemplate.update(sql, new PreparedStatementSetter(){
			public void setValues(PreparedStatement ps) throws SQLException {
				ps.setInt(1, placovuAppDto.getStatusCode());
				ps.setLong(2, placovuAppDto.getId());
				
			}
		});
	}
	
	public void insertPlacovuApp(final PlacovuAppDto placovuAppDto)
	{
		String sql = "INSERT INTO placovu_app (service_name,service_type ,ip_address ,domain_name) VALUES (?, ?, ?, ?);";
		//System.out.println(placovuAppDto.getStatusCode());
		System.out.println(sql);
		jdbcTemplate.update(sql, new PreparedStatementSetter(){
			public void setValues(PreparedStatement ps) throws SQLException {
				ps.setString(1, placovuAppDto.getServiceName());
				ps.setString(2, placovuAppDto.getServiceType());
				ps.setString(3, placovuAppDto.getDomainName());
				ps.setString(4, placovuAppDto.getIpAddress());
				
			}
		});
	}
	
	
	public void editPlacovuApp(final PlacovuAppDto placovuAppDto)
	{
		String sql = "update placovu_app set status_code = ? where id = ?";
		System.out.println(placovuAppDto.getStatusCode());
		System.out.println(sql);
		jdbcTemplate.update(sql, new PreparedStatementSetter(){
			public void setValues(PreparedStatement ps) throws SQLException {
				ps.setInt(1, placovuAppDto.getStatusCode());
				ps.setLong(2, placovuAppDto.getId());
				
			}
		});
	}
	
	
	
}
