package com.placovupingapp.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.placovupingapp.dto.UserDto;
import com.placovupingapp.rowmapper.UserMapper;

@Repository
public class UserDao {
	private JdbcTemplate jdbcTemplate;
    public void setJdbcTemplate(JdbcTemplate template) {  
        this.jdbcTemplate = template;  
    } 
	public List<UserDto> getUserDtos() {
		List<UserDto> userDtos = null;
		try{
			String sql = "select *from user";
			userDtos = jdbcTemplate.query(sql,new UserMapper());
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return userDtos;
	}

}
