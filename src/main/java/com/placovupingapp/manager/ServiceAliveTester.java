package com.placovupingapp.manager;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.placovupingapp.dao.PlacovuAppDao;
import com.placovupingapp.dto.PlacovuAppDto;
import com.placovupingapp.mailmanager.Email;
import com.placovupingapp.mailmanager.EmailMessage;
import com.placovupingapp.mailmanager.EmailSender;
@Service 
public class ServiceAliveTester {
	@Autowired EmailSender emailSender;
	@Autowired Email email;
	
	 static Boolean OntrackHealthWebServiceEmail = false;
	 static Boolean OntrackHealthMailServiceEmail = false;
	 static Boolean OntrackHealthDatabaseServiceEmail = false;
	private static final Logger logger = Logger.getLogger(ServiceAliveTester.class);
	public void testWebSeverRunning (PlacovuAppDao placovuAppDao, PlacovuAppDto dto)
	{
	    Boolean sendEmail = false;
		try{
		URL url = new URL( dto.getDomainName() );
		HttpURLConnection httpConn =  (HttpURLConnection)url.openConnection();
		httpConn.setInstanceFollowRedirects( false );
		httpConn.setRequestMethod( "HEAD" ); 
		
		    httpConn.connect();
		     System.out.println( "service : " + dto.getServiceName() + " , status code: " + httpConn.getResponseCode());
		    if(httpConn.getResponseCode() != dto.getStatusCode() && httpConn.getResponseCode() == 404)
		    	sendEmail = true;
		    if(httpConn.getResponseCode() != dto.getStatusCode())
		    {
		    	 dto.setStatusCode(httpConn.getResponseCode());
		    	placovuAppDao.updatePlacovuApp(dto);
		    }
		}catch(Exception e){
		     System.out.println( "service : " + dto.getServiceName() + " is down ");
		     if(dto.getStatusCode() != 404)
		     {
		    	 sendEmail = true;
		    	 dto.setStatusCode(404);
		    	 placovuAppDao.updatePlacovuApp(dto);
		     }
		}
		if(sendEmail)
		{
			 EmailMessage emailMessage = new EmailMessage();
			 emailMessage.setReceiverEmail(email.getReceiverEmail());
			 emailMessage.setSubject(dto.getDomainName() + " is down");
			 emailMessage.setMessage("Dear sir, \\n " + "your " + dto.getDomainName() + " is down. IP address: " + dto.getIpAddress() );
			 //emailSender.sendEmail(emailMessage);
		}
	    
	}
	
	public  void testWindowsService(PlacovuAppDao placovuAppDao,PlacovuAppDto dto)
	{
		//System.out.println("domain: "+dto.getDomainName() + "    service name: " + dto.getServiceName());
		String line;
		String pidInfo ="";
		Boolean sendEmail = false;
		try{

			Process p =Runtime.getRuntime().exec(System.getenv("windir") +"\\system32\\"+"tasklist.exe");
	
			BufferedReader input =  new BufferedReader(new InputStreamReader(p.getInputStream()));
	
			while ((line = input.readLine()) != null) {
				//System.out.println(line);
			    pidInfo+=line; 
			}
			input.close();
			if(pidInfo.contains(dto.getDomainName()))
			{
				//System.out.println(dto.getDomainName());
			    System.out.println("your system  "+dto.getDomainName()+" is running");
			    if(dto.getStatusCode() != 200)
			    {
			    	dto.setStatusCode(202);
			    	placovuAppDao.updatePlacovuApp(dto);
			    }
			    	
			}
			else
			{
				System.out.println("your system "+dto.getDomainName()+" is not running");
				if(dto.getStatusCode() != 404)
			    {
			    	dto.setStatusCode(404);
			    	placovuAppDao.updatePlacovuApp(dto);
			    	sendEmail = true;
			    }
			}
		}
		catch(Exception e)
		{
			System.out.println("Not Running");
		}
		if(sendEmail)
		{
			
			 EmailMessage emailMessage = new EmailMessage();
			 emailMessage.setReceiverEmail(email.getReceiverEmail());
			 emailMessage.setSubject(dto.getDomainName() + " is down");
			 emailMessage.setMessage("Dear sir, \\n " + "your " + dto.getDomainName() + " is down. IP address: " + dto.getIpAddress() );
			// emailSender.sendEmail(emailMessage);
			 
			 
		}
	}
	public void testWebSeverRunningUsingRestTemplate (String url,boolean databaseUrl)
	{
	   boolean sendEmail = false;
		try{
			RestTemplate restTemplate = new RestTemplate();
			String resourceUrl= url;
			ResponseEntity<String> response = restTemplate.getForEntity(resourceUrl, String.class);
		    System.out.println(url+" status code is "+response.getStatusCode().value()); 
		    if (response.getStatusCode()!=HttpStatus.OK)
		    {
		    	sendEmail = true;
		    
		    }
		    else
		    {
		    	if(databaseUrl)
		    		OntrackHealthDatabaseServiceEmail = false;
		    	else
		    		OntrackHealthWebServiceEmail = false;
		    }
		}
		catch(Exception e){
			sendEmail= true;
		    System.out.println( "service : is down ");
		}
		if(sendEmail)
		{
			 EmailMessage emailMessage = new EmailMessage();
			 emailMessage.setReceiverEmail(email.getReceiverEmail());
			 emailMessage.setSubject(url+" is down");
			 emailMessage.setMessage("Dear sir, \\n " + "your  "+url+" is down " );
			 
			 if(databaseUrl && OntrackHealthDatabaseServiceEmail == false)
			 {
				 emailSender.sendEmail(emailMessage);
				 OntrackHealthDatabaseServiceEmail = true;
			 }
			 else if(!databaseUrl && OntrackHealthWebServiceEmail == false)
			 {
				 emailSender.sendEmail(emailMessage);
				 OntrackHealthWebServiceEmail = true;
			 }
				 
		}
	}

	public void testWindowsService(String emailServiceName) {
				String line;
				String pidInfo ="";
				Boolean sendEmail = false;
				try{

					Process p =Runtime.getRuntime().exec(System.getenv("windir") +"\\system32\\"+"tasklist.exe");
					BufferedReader input =  new BufferedReader(new InputStreamReader(p.getInputStream()));
					while ((line = input.readLine()) != null) {
						//System.out.println(line);
					    pidInfo+=line; 
					}
					input.close();
					if(pidInfo.contains(emailServiceName))
					{
						//System.out.println(dto.getDomainName());
					    logger.info("your system  "+emailServiceName+" is running");
					    OntrackHealthMailServiceEmail = false;
					}
					else
					{
						logger.info("your system "+emailServiceName+" is not running");
				        sendEmail = true;    
					}
				}
				catch(Exception e)
				{
					logger.info("Not Running");
					sendEmail = true;
				}
				if(sendEmail)
				{
					 EmailMessage emailMessage = new EmailMessage();
					 emailMessage.setReceiverEmail(email.getReceiverEmail());
					 emailMessage.setSubject(emailServiceName + " is down");
					 emailMessage.setMessage("Dear sir, \\n " + "your " +emailServiceName +"is not working");
					 
					 if(!OntrackHealthMailServiceEmail)
					 {
						 emailSender.sendEmail(emailMessage);
						 OntrackHealthMailServiceEmail = true;
					 }
				}
	}
}




