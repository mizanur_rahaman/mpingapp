package com.placovupingapp.manager;

import org.springframework.beans.factory.annotation.Autowired;

import com.placovupingapp.dto.PlacovuAppRestDto;
import com.placovupingapp.service.PlacovuAppService;

public class PingAppTaskScheduler
{
  
    @Autowired PlacovuAppService placovuServiceService;
    @Autowired PlacovuAppRestDto  placovuAppRestDto;
   public void run()
   {
	   System.out.println("PingApp executed at every 30 seconds. Current time is :: "+ System.currentTimeMillis());
	   //placovuServiceService.testAppService();
	   placovuServiceService.testWebSeverRunningUsingRestTemplate(placovuAppRestDto.getOntrackHealthUrl(),false);
	   placovuServiceService.testWebSeverRunningUsingRestTemplate(placovuAppRestDto.getOntrackDatabaseUrl(),true);
	   placovuServiceService.testWindowsService(placovuAppRestDto.getEmailServiceName());
   }
}