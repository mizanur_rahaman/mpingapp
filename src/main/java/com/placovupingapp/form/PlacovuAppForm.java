package com.placovupingapp.form;

public class PlacovuAppForm {
	public String servciceName;
	public String serviceType;
	public String domainName;
	public String ipAddress;
	public String getServciceName() {
		return servciceName;
	}
	public void setServciceName(String servciceName) {
		this.servciceName = servciceName;
	}
	public String getServiceType() {
		return serviceType;
	}
	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}
	public String getDomainName() {
		return domainName;
	}
	public void setDomainName(String domainName) {
		this.domainName = domainName;
	}
	public String getIpAddress() {
		return ipAddress;
	}
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	

}
